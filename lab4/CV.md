Radja Nainggolan "Kralj"
Profesionalni nogometaš

Radja Nainggolan (Antwerpen, 4. svibnja 1988.) belgijski je nogometaš koji trenutačno igra za talijanski nogometni klub Cagliari na posudbi iz Inter Milana.

Nainggolan je započeo svoju profesionalnu karijeru Italiji u Piacenzi. Do danas je igrao isključivo u talijanskim klubovima.

Nainggolan je predstavljao Belgiju na Europsko prvenstvo u 2016. godini. 

Nainggolan je rođen u Antwerpenu kao dijete majke Belgijanke flamanskog porijekla, koja je odhranila zajedno sa njim njegovu sestru i tri polubrata.Otac Marianus Nainggolan, protestant porijeklom iz Indonezije, je napustio Nainggolana u djetinjstvu.[3]

Nainggolanova majka je preminula u 2010. godini. Odgojen je kao katolik i zna nizozemski, engleski, talijanski jezik i razumije francuski.

Karijera:

Piacenza

Nainggolan je stigao u Piacenzi u 2005. godini sa 17 godina. U 2008./09. sezoni je Nainggolan odigrao 38 utakmica za momčad iz Emilia-Romagne, gdje je zabio tri gola.

Cagliari

U siječnju 2010. godini je Belgijanac otišao na posudbu u Cagliari u Serie A. Mjesec dana kasnije je debitirao za Cagliari u 3:0 porazu protiv Inter Milana. Krajem te sezone je Cagliari kupio Nainggolana od Piacenze. U listopadu 2013. godine je veznjak produžio svoj ugovor sa Cagliarijem do 2016. godine. U siječnju godinu poslije je poslan na posudbu u A.S. Romu za tri milijuna eura.

A.S. Roma

Nianggolan je debitirao za rimski klub u siječnju 2014. godine u Coppa Italiji protiv Sampdorije. U veljači te godine je Nainggolan zabio svoj prvi pogodak za A.S. Romu u pobjedi protiv Bologne. Drugi podogak je zabio protiv Fiorentine u travnju 2014. godine. Rimljani su sa tom pobjedom osigurali plasman u Ligi prvaka. U sljedećoj sezoni je Nainggolan definitivno potpisao ugovor sa A.S. Romom za 9 milijuna eura.
Tijekom ljetnog prijelaznog roka u 2016. godini je Chelsea ponudio 28 milijuna eura za Nainggolana. Međutim Belgijanac je potvrdio da ostaje u A.S. Romi.
Protiv Palerma je Belgijanac odigrao svoju 100. ligašku utakmicu za Giallorossi u četrnaestom kolu Serie A. U gradskom derbiju protiv S.S. Lazija je Nianggolan zabio za 0:2 na domaćem terenu u prosincu 2016.
Belgijanac je produžio svoj ugovor sa rimskim klubom do 2021. u srpnju 2017. godine.

Inter Milan

U lipnju 2018. godine je Nainggolan prešao iz Rome u Inter Milan za 38 milijuna eura. Bosanskohercegovački reprezentativac Edin Džeko je se oprostio od svog suigrača sa emotivnom porukom na društvenim mrežama.[19] Zabio je pogodak tijekom svog debija protiv Bologne. U kolovozu 2019. godine je se vratio u Cagliari na posudbu. 