 #ukljucivanje biblioteke pygame
import pygame

def next_img(image):
    if image==bg_img:
        image=bg_img1
    elif image==bg_img1:
        image=bg_img2
    elif image==bg_img2:
        image=bg_img3
    elif image==bg_img3:
        image=bg_img4
    elif image==bg_img4:
        image=bg_img5
    elif image==bg_img5:
        image=bg_img6
    elif image==bg_img6:
        image=bg_img7
    elif image==bg_img7:    
        image=bg_img
    return image
pygame.init()
# definiranje konstanti za velicinu prozora
WIDTH = 1500
HEIGHT = 600
RED=[255,0,0]
GREEN=[0,255,0]
BLUE=[0,0,255]
TIRKIZ=[64,224,208]
tamnocijan=[0,139,139]
magenta=[255,0,255]

bg_img=pygame.image.load("golf1.jpeg")
bg_img=pygame.transform.scale(bg_img,(WIDTH,HEIGHT))
bg_img1=pygame.image.load("golf2.jpg")
bg_img1=pygame.transform.scale(bg_img1,(WIDTH,HEIGHT))
bg_img2=pygame.image.load("golf3.jpg")
bg_img2=pygame.transform.scale(bg_img2,(WIDTH,HEIGHT))
bg_img3=pygame.image.load("golf4.jpg")
bg_img3=pygame.transform.scale(bg_img3,(WIDTH,HEIGHT))
bg_img4=pygame.image.load("golf5.jpg")
bg_img4=pygame.transform.scale(bg_img4,(WIDTH,HEIGHT))
bg_img5=pygame.image.load("golf6.jpg")
bg_img5=pygame.transform.scale(bg_img5,(WIDTH,HEIGHT))
bg_img6=pygame.image.load("golf7.jpeg")
bg_img6=pygame.transform.scale(bg_img6,(WIDTH,HEIGHT))
bg_img7=pygame.image.load("golf8.jpg")
bg_img7=pygame.transform.scale(bg_img7,(WIDTH,HEIGHT))

myfont = pygame.font.SysFont('Arial', 40)
golf1_text = myfont.render("Prva generacija Golfa 1974. ", False, RED)
golf2_text = myfont.render("Druga generacija Golfa 1983. ", False, BLUE)
golf3_text = myfont.render("Treća generacija Golfa 1991.", False, TIRKIZ)
golf4_text = myfont.render("Četvrta generacija Golfa 1997. ", False, BLUE)
golf5_text = myfont.render("Peta generacija Golfa 2003. ", False, RED)
golf6_text = myfont.render("Šesta generacija Golfa 2008. ", False, BLUE)
golf7_text = myfont.render("Sedma generacija Golfa 2012. ", False, RED)
golf8_text = myfont.render("Osma generacija Golfa 2020. ", False, TIRKIZ)

# tuple velicine prozora
size = (WIDTH, HEIGHT)
 
#definiranje novog ekrana za igru
screen = pygame.display.set_mode(size)
#definiranje naziva prozora
pygame.display.set_caption("VW Golf")
 
clock = pygame.time.Clock()
i=0
duration=10000
bg_color=tamnocijan
bg=bg_img
done = False
while not done:
    #event petlja
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True
        if event.type ==pygame.KEYDOWN:
            if event.key==pygame.K_SPACE:
                bg=next_img(bg)               
        
    if duration<0 and i == 0:
        duration=10000
        i = 1
    elif duration<0 and i == 1:
        duration=10000
        i = 2
    elif duration<0 and i == 2:
        duration=10000
        i = 3
    elif duration<0 and i == 3:
        duration=10000
        i = 4
    elif duration<0 and i == 4:
        duration=10000
        i = 5
    elif duration<0 and i == 5:
        duration=10000
        i = 6
    elif duration<0 and i == 6:
        duration=10000
        i = 7
    elif duration<0 and i == 7:
        duration=10000 
        i = 0                     
        

    if i == 0:
        screen.blit(bg_img,(0,0))
        screen.blit(golf1_text,(0,0))
    elif i == 1:
        screen.blit(bg_img1,(0,0))
        screen.blit(golf2_text,(0,0))
    elif i == 2:
        screen.blit(bg_img2,(0,0))
        screen.blit(golf3_text,(0,0))   
    elif i == 3:
        screen.blit(bg_img3,(0,0))
        screen.blit(golf4_text,(0,0))        
    elif i == 4:
        screen.blit(bg_img4,(0,0))
        screen.blit(golf5_text,(0,0))
    elif i == 5:
        screen.blit(bg_img5,(0,0))
        screen.blit(golf6_text,(0,0))
    elif i == 6:
        screen.blit(bg_img6,(0,0))
        screen.blit(golf7_text,(0,0))
    elif i == 7:
        screen.blit(bg_img7,(0,0))
        screen.blit(golf8_text,(0,0))  




    pygame.display.flip()
   
    #ukoliko je potrebno ceka do iscrtavanja




    #iduceg framea kako bi imao 60fpsa
    time= clock.tick(60)
    duration=duration-time
    #print(duration)
pygame.quit()
