# ukljucivanje biblioteke pygame
import pygame
def change_bg(color):
    if color == tirkiz:
                color = red    
    elif color == red:
                color = magenta
    elif color == magenta:
                color = tirkiz
    return color   
def next_img(image):
    if image == bg_img:
        image = bg_img1   
    elif image  == bg_img1:
        image = bg_img2
    elif image == bg_img2:
        image = bg_img
    return image   
pygame.init()
# definiranje konstanti za velicinu prozora

WIDTH = 1024
HEIGHT = 600
red = [255,0,0]
green = [0,255,0]
blue = [0,0,255]
tirkiz = [64,244,208]
magenta = [255,0,255]

bg_img = pygame.image.load("bg.jpg")
bg_img = pygame.transform.scale(bg_img,(WIDTH,HEIGHT))
bg_img1 = pygame.image.load("bg1.jpg")
bg_img1 = pygame.transform.scale(bg_img1,(WIDTH,HEIGHT))
bg_img2= pygame.image.load("bg2.jpg")
bg_img2 = pygame.transform.scale(bg_img2,(WIDTH,HEIGHT))

# tuple velicine prozora
size = (WIDTH, HEIGHT)
 
#definiranje novog ekrana za igru
screen = pygame.display.set_mode(size)
#definiranje naziva prozora
pygame.display.set_caption("Nasa kul igra")
 
clock = pygame.time.Clock()
i = 0
duration = 1000
bg_color = tirkiz
bg = bg_img
done = False
while not done:
    #event petlja
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True

        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_SPACE:
                if bg_color == tirkiz:
                    bg_color = red    
                elif bg_color == red:
                    bg_color = magenta
                elif bg_color == magenta:
                    bg_color = tirkiz        
        i += 1
        if duration < 0:
            duration = 1000
            bg = next_img(bg)
            
    screen.fill((bg_color))
    screen.blit(bg,(0,0))
    pygame.display.flip()
    
    #ukoliko je potrebno ceka do iscrtavanja
    #iduceg framea kako bi imao 60fpsa
    time = clock.tick(60)
    duration=duration-time
    #print(time)

pygame.quit()